import io
import json
from collections import Counter, OrderedDict

file_content = io.open("dutch_words.txt", 'r',encoding='utf-8')
word_list = []
for word in file_content:
    #word = word.encode().decode('utf-8')
    word = word.strip("\n")
    word = word.lower()
    word_list.append(word)
    
words_dict = dict(Counter(word_list))
file_content.close()
with open('../rasa/data/dutch_words.json', 'w') as fp:
    json.dump(words_dict, fp, skipkeys=False, ensure_ascii=True)
## Updated Repository Information

This repository only contains the training data for the NLU, configurations voor training the NLU and rules. All other technical files, including Docker files, are located in the following repository:

[**Virtuele Gemeente Assistent - Rasa**](https://gitlab.com/virtuele-gemeente-assistent/rasa)